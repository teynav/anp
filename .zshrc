# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH
export EDITOR=nvim
export VISUAL=nvim
# Path to your oh-my-zsh installation.
export ZSH="/home/navtey/.oh-my-zsh"
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus
# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="agnoster"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(zsh-completions zsh-autosuggestions zsh-syntax-highlighting sudo cp)
autoload -Uz compinit && compinit
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
export HISTCONTROL=ignoredups:erasedups
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt extendedglob
typeset -A abbrevs
abbrevs=(
  "pac"   "sudo pacman -Syuu "
  "par"   "sudo pacman -Rsn"
  "sys" "sudo systemctl restart "
   "syt" "sudo systemctl stop"
   "n" "nvim"
   "fdd" 'find . -iname "*__CURSOR__*"'
   "gp" '| grep "*__CURSOR__*"'
   "aak" "| awk '{ print \$__CURSOR__}'"
   "conf" 'git --git-dir=$HOME/.navtey/ --work-tree=$HOME '
   "aur" 'pikaur -Syuu'
)

for abbr in ${(k)abbrevs}; do
  alias $abbr="${abbrevs[$abbr]}"
done

magic-abbrev-expand() {
  local MATCH
  LBUFFER=${LBUFFER%%(#m)[_a-zA-Z0-9]#}
  command=${abbrevs[$MATCH]}
  LBUFFER+=${command:-$MATCH}

  if [[ "${command}" =~ "__CURSOR__" ]]; then
    RBUFFER=${LBUFFER[(ws:__CURSOR__:)2]}
    LBUFFER=${LBUFFER[(ws:__CURSOR__:)1]}
  else
    zle self-insert
  fi
}

# magic-abbrev-expand-and-execute() {
#  magic-abbrev-expand
#  zle backward-delete-char
#  zle accept-line
#}

# no-magic-abbrev-expand() {
#  LBUFFER+=' '
# }

zle -N magic-abbrev-expand
#zle -N magic-abbrev-expand-and-execute
#zle -N no-magic-abbrev-expand

bindkey " " magic-abbrev-expand
#bindkey "^M" magic-abbrev-expand-and-execute
#bindkey "^x " no-magic-abbrev-expand
#bindkey -M isearch " " self-insert
catch_signal_usr1() {
  trap catch_signal_usr1 USR1
  rehash
}
trap catch_signal_usr1 USR1

if [[ "$(tty)" == "/dev/tty1" ]]
 then
   startx  
  exit
fi

##############################################3
#My prompt
sepp=""
sepp2=""
lep=""
autoload -Uz vcs_info
#zstyle ':vcs_info:*' actionformats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
#zstyle ':vcs_info:*' formats '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
#zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
#zstyle ':vcs_info:*' disable bzr cdv darcs mtn svk tla
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT=\$vcs_info_msg_0_
zstyle ':vcs_info:git:*' formats '%F{53}%f%F{108}%K{53}%u %c %f%F{blue}%k%f%K{blue}%F{black} %b %f%F{101}%f%k%K{101}%F{88} %r %f%k'
zstyle ':vcs_info:git:*' check-for-changes true 
zstyle ':vcs_info:git:*' check-for-staged-changes true
zstyle ':vcs_info:git:*' stagedstr "❕" 
zstyle ':vcs_info:git:*' unstagedstr "❗️"
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' disable-patterns "${(b)HOME}/.navtey(|/*)"
#PS1='%F{5}[%F{2}%n%F{5}] %F{3}%3~ ${vcs_info_msg_0_}%f%# '
PROMPT='%F{black}%K{white} $USER %f%k%K{blue}$sepp%f%B%K{blue}%F{black} %2~ %f%b%k%F{blue}$sepp %f%(?.. ❌ )'
RPROMPT='${vcs_info_msg_0_}%1'
source /usr/share/doc/pkgfile/command-not-found.zsh
##Comment

source /home/navtey/.config/broot/launcher/bash/br
