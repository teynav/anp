2.Conjuctiva

Date: 02/08/2020

Hours: 08:10

Details:

* * *

# Parts

1.  Palpebral conjuctiva => Lines the lid
    - Marginal Conjuctiva=> Transition zone b/w conjuctiva proper and skin
    - Tarsal Conjuctiva => Thin Trasnparent highly vascular, whole upper lid , in lower only half width of Tarsal plate
    - Orbital Conjuctiva=> Loose lies b/w Tarsal plate and fornix
2.  Conjuctival fornix => Joines Bulbar w/ Palpebral; Broken at caruncle and plica semilunaris
3.  Bulbar conjuctiva

* * *

# Histology

1.  Epithelium :> Non Keratinized Squamous epithelium
2.  Stroma :> MALT/CALT, develop 3/4m > life
3.  Fibrous tissue :> Blends with tenon's capsule

- Goblet cell max density in 'inferonasal bulbar conjuctiva' & fornices , decreased in Vitamin A deficiency
- Lymphoid hyperplasia => follicle
- ![20200802_081234.jpg](resources/a456cc27bffa4855aa639422b7b7a1a1.jpg)

### Gands of conjuctiva

```
 1. Mucin 
     - Goblet : located within the epithelium
     - Crypts of Henle : in tarsal conjuctiva
     - Glands of Manz : limbal conjuctiva
 2. Accesory lacrimal
     - Glands of Krause : 48+8 upper+lower fornix
     - Glands of Wolfring : in tarsus

```

* * *

# Inflammation of Conjuctiva

## Infective conjuctivitis

![083e42e8bac0900ec68c86af9bff05d6.png](resources/48747fa62b5047d9960805f7da50a1e5.png)

1.  Bacterial
    
    - Acute bacterial conjuctivitis
        - Symptoms
            - Discomfort foreign body
            - Mild photophobia
            - Coloured halos (due to mucus)
            - Sticking together lids
        - Signs
            - Flakes of mucopus
            - Conjuctival congestion
            - Chemosis
            - Petechial haemorrage in Pneumococcus
            - Cilia usually matted together
        - Course
            - Usually bilateral
            - maximum at 3-4 day
            - Usually resolve or chronic catarrhal conjuctivitis
        - Complication
            - Ulceration, Blepharitis, Darcocystitis
        - MCC
            - Staph aureus
            - Pneumococcus
            - Koch-Wheel Bacillus
        - Tx
            - Topical antibiotic
                1.  1% cholramphenicol
                2.  0.3% gentamicin or tobramycin if not responds
                3.  ciprofloxacin 0.3%
                4.  moxifloxacin 0.5%
                5.  ofloxacin 0.3%
            - Irrigation of Conjuctival sac
            - Dark googles
            - No bandage / steroid
    - HyperAcute bacterial conjuctivitis Voilent immune response to gonococcus
        1.  Adult prulent
            - Symptoms
                - Pain moderate to severe
                - Prulent copious discharge
                - Blurring vision
                - Sticking eyelids
                - Mild photophobia
            - Sign
                - Eyelids thick swollen
                - Discharge is thich prulent, trickling down eyes
                - Marked chemosis, congestion
            - Complication
                - Perforations and ulcerations
                - Iridocyclitis : not common
                - Systemic complication
            - Treatment
                - Any of these is adopted Systematically
                    - 3rd gen cephalosporin
                        - Cefoxitin 1g IV qid 5d
                        - Cetriaxone 500mg IV qid 5d
                    - Quinilones : Norfloxacin 1.2g O qid 5d
                    - Spectinomycin 2g IM 3d
                - Topical
                    - ofloxacin ciprofloxacin tobramycin
                - Irrigation w/ saline
                - atropine 1% if cornea is involved
        2.  Opthalmia neonatorum
    - Chronic bacterial conjuctivitis
        - Also known as Chronic Catarrhal connjuctivitis
        - Predisposing factor
            - Chronic exposure to dust
            - Alcohol abuse, insomnia
            - Eye strain
            - Local cause of irritation ex trichiasis, foreign body
        - Causative organism
            - S aureus
            - Gram -ve rods : E coli, K pneumoniae, P mirabilis
        - Source and mode on inf
            - continuation of acute disease
            - chronic darcocystitis / rhinitis
        - Symptoms
            - Burning and Glittiness of eyes especially in evening
            - Mild Chronic redness of eyes
            - Waterning off and on
            - Feeling of sleepiness
            - Difficulty in keeping eyes open
        - Sign
            - Congestion of posterior conjuctival vessels
            - Mild papillary hypertrophy of the palpebral conjuctiva
            - Lid margin is congested
        - Tx
            - Topical antibiotics
            - Astringent eye drops Zn-Boric drops
    - Angular bacterial conjuctivitis
        - MCC
            - Moraxella axenfield
            - usually nasal cavity is source
        - Pathology
            - Produces proteolytic enzyme which macernates epithelium
            - It gets collected at the angle due to tears
            - Mild chronic inflammation is seen, skin shows ezematous changes
        - Symptoms
        - Sign
            - Hyperaemia of bulbar conjuctiva near canthi
        - Tx
            - Oxtertracycline 1%
            - Excoriation of skin around angle
            - Zinc lotion
    - Chlamydial Conjuctivitis
        - Elementary body initiates infection
        - Trachoma
            - It is chronic keratoconjuctivitis
            - Leading cause of blindness 15-20% 2nd only to cataract
            - Hyperendemic trachoma : blinding trachoma
            - Paratrachoma : inclusion trachoma
            - Risk F.
                - Age : mostly infancy
                - Sex : female more and severe
                - Race : more in jew less in Negroes
            - Source : Mainly conjuctival discharge
            - Modes of Infection
                - Direct
                - Fomites
            - Prevalance
                - High in north africa, Middle east
            - Sign
                - Mild symptoms in pure infections
                - Congestion in upper conjuctiva
                - Conjuctival follicles :
                    - Sago grain structure in upper tarsal conjuctiva maybe  
                        in lower fornix and plica semilunaris
                    - Pathological structure has ==mononuclear histiocycte==
                    - Also has multinucleated ==Leber== cells
                 - Papillary Hyperplasia : Dilated blood vessel
             - Corneal signs
                 -  Superficial keratitis
                 - ==Herbert follicles== refers to typical follicles in limbal area
                 - Progressive ==pannus== : infiltration of cornea w/ vascularization is seen in upper parts. Vessel lie b/w epithelium & Bowman's membrane. latter is destroyed too. 
2.  Viral
3.  Opthalmia neonatorum
4. Granulomatous Conjuctiva
    
## Allergic Conjuctivitis
