set nocompatible
syntax on
filetype plugin on
set path+=**
set wildmenu
set relativenumber
set number
syntax on
command! MakeTags !ctags -R .
nnoremap <BS> :tabprevious<CR>
nnoremap <Tab> :tabnext<CR>
nnoremap <C-N> :tabe<CR>
nnoremap <C-H> :NERDTree<CR>
nnoremap <Space> :FZF<CR>
inoremap <silent> <C-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <C-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>
autocmd FileType vimwiki set ft=markdown
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
set clipboard=unnamedplus
set mouse=a
let g:airline_theme='atomic'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
nnore map <silent> vv <C-w>v
map! <C-v>GA Γ
map! <C-v>DE Δ
map! <C-v>TH Θ
map! <C-v>LA Λ
map! <C-v>XI Ξ
map! <C-v>PI Π
map! <C-v>SI Σ
map! <C-v>PH Φ
map! <C-v>PS Ψ
map! <C-v>OM Ω
map! <C-v>al α
map! <C-v>be β
map! <C-v>ga γ
map! <C-v>de δ
map! <C-v>ep ε
map! <C-v>ze ζ
map! <C-v>et η
map! <C-v>th θ
map! <C-v>io ι
map! <C-v>ka κ
map! <C-v>la λ
map! <C-v>mu μ
map! <C-v>xi ξ
map! <C-v>pi π
map! <C-v>rh ρ
map! <C-v>si σ
map! <C-v>ta τ
map! <C-v>ps ψ
map! <C-v>om ω
map! <C-v>ph ϕ
" Math {{{2
 map! <C-v>ll →
 map! <C-v>hh ⇌
 map! <C-v>kk ↑
 map! <C-v>jj ↓
 map! <C-v>= ∝
 map! <C-v>~ ≈
 map! <C-v>!= ≠
 map! <C-v>!> ⇸
 map! <C-v>~> ↝
 map! <C-v>>= ≥
 map! <C-v><= ≤
 map! <C-v>0  °
 map! <C-v>ce ¢
 map! <C-v>*  •
 map! <C-v>co ⌘
" Subscript and Superscript {{{2
inoremap <leader>1 ~1~
inoremap <leader>2 ~2~
inoremap <leader>3 ~3~
inoremap <leader>4 ~4~
inoremap <leader>5 ~5~
inoremap <leader>6 ~6~
inoremap <leader>7 ~7~
inoremap <leader>8 ~8~
inoremap <leader>9 ~9~
inoremap <leader>== ^+^
inoremap <leader>=2 ^2+^
inoremap <leader>=3 ^3+^
inoremap <leader>-- ^-^
inoremap <leader>-2 ^2-^
inoremap <leader>-3 ^3-^

