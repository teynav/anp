call plug#begin()
Plug 'preservim/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'liuchengxu/vim-which-key'
Plug 'rbgrouleff/bclose.vim'
Plug 'francoiscabrol/ranger.vim'
Plug 'morhetz/gruvbox'
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif
let g:deoplete#enable_at_startup = 1
"set colorscheme gruvbox eww don't do this one tey :/ 
"autocmd vimenter * colorscheme gruvbox
"set termguicolors

call plug#end()
syntax on
set t_Co=256
filetype plugin indent on
set nocompatible
syntax on
"filetype plugin on
set path+=**
set wildmenu
set relativenumber
set number
syntax on
command! MakeTags !ctags -R .
nnoremap <A-Left> :tabprevious<CR>
nnoremap <A-Right> :tabnext<CR>
nnoremap <C-N> :tabe<CR>
nnoremap <A-Up> :NERDTreeToggle<CR>
nnoremap <A-Down> :FZF<CR>
inoremap <silent> <C-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <C-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>
let mapleader = " "
nnoremap <leader>h <C-w>h
nnoremap <leader>j <C-w>j
nnoremap <leader>k <C-w>k
nnoremap <leader>l <C-w>l
nnoremap <leader><Left> <C-w>h
nnoremap <leader><Down> <C-w>j
nnoremap <leader><UP> <C-w>k
nnoremap <leader><Right> <C-w>l
nnoremap <leader>b :bnext<CR>
nnoremap <leader>q <C-w>q
nnoremap <leader>g <C-w>s
nnoremap <leader>v <C-w>v
nnoremap <leader><leader> :tabnew<CR>
nnoremap "p :reg <bar> exec 'normal! "'.input('>').'p'<CR>
let maplocalleader = ","

autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
set clipboard=unnamedplus
set mouse=a
"let g:airline_theme='atomic'
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
nnore map <silent> vv <C-w>v
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
inoremap <localleader><localleader> <Esc>pa
inoremap <localleader>. <Esc>y$i
inoremap <localleader>/ <Esc>ddi
inoremap <localleader>; <Esc>:reg <bar> exec 'normal! "'.input('>>')'p$a'<CR>

source $HOME/.config/nvim/themes/airline.vim
source $HOME/.config/nvim/themes/nerdtree.vim
source $HOME/.config/nvim/themes/vm.vim


"autocmd FileType vimwiki set ft=markdown

map! <C-v>GA Γ
map! <C-v>DE Δ
map! <C-v>TH Θ
map! <C-v>LA Λ
map! <C-v>XI Ξ
map! <C-v>PI Π
map! <C-v>SI Σ
map! <C-v>PH Φ
map! <C-v>PS Ψ
map! <C-v>OM Ω
map! <C-v>al α
map! <C-v>be β
map! <C-v>ga γ
map! <C-v>de δ
map! <C-v>ep ε
map! <C-v>ze ζ
map! <C-v>et η
map! <C-v>th θ
map! <C-v>io ι
map! <C-v>ka κ
map! <C-v>la λ
map! <C-v>mu μ
map! <C-v>xi ξ
map! <C-v>pi π
map! <C-v>rh ρ
map! <C-v>si σ
map! <C-v>ta τ
map! <C-v>ps ψ
map! <C-v>om ω
map! <C-v>ph ϕ
" Math {{{2
 map! <C-v>ll →
 map! <C-v>hh ⇌
 map! <C-v>kk ↑
 map! <C-v>jj ↓
 map! <C-v>= ∝
 map! <C-v>~ ≈
 map! <C-v>!= ≠
 map! <C-v>!> ⇸
 map! <C-v>~> ↝
 map! <C-v>>= ≥
 map! <C-v><= ≤
 map! <C-v>0  °
 map! <C-v>ce ¢
 map! <C-v>*  •
 map! <C-v>co ⌘
" top, right, bottom, left border in popups

 " Subscript and Superscript {{{2

" dark0 + gray
"let g:terminal_color_0 = '#282828'
"let g:terminal_color_8 = '#928374'

" neurtral_red + bright_red
"let g:terminal_color_1 = '#cc241d'
"let g:terminal_color_9 = '#fb4934'

" neutral_green + bright_green
"let g:terminal_color_2 = '#98971a'
"let g:terminal_color_10 = '#b8bb26'

" neutral_yellow + bright_yellow
"let g:terminal_color_3 = '#d79921'
"let g:terminal_color_11 = '#fabd2f'

" neutral_blue + bright_blue
"let g:terminal_color_4 = '#458588'
"let g:terminal_color_12 = '#83a598'

" neutral_purple + bright_purple
"let g:terminal_color_5 = '#b16286'
"let g:terminal_color_13 = '#d3869b'

" neutral_aqua + faded_aqua
"let g:terminal_color_6 = '#689d6a'
"let g:terminal_color_14 = '#8ec07c'

" light4 + light1
"let g:terminal_color_7 = '#a89984'
"let g:terminal_color_15 = '#ebdbb2'
"autocmd vimenter * colorscheme gruvbox
"autocmd VimLeave * silent !tmux set status 
