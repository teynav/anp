highlight default link WhichKey          Operator
highlight default link WhichKeySeperator DiffAdded
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function
let g:which_key_use_floating_win = 0
" Create map to add keys to
let g:which_key_map =  {}
" Define a separator
let g:which_key_sep = '→'
" set timeoutlen=100
" Hide status line
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler
nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>
let g:which_key_map['h'] = [ '<C-w>h' , 'which_key_ignore' ]
let g:which_key_map['j'] = [ '<C-w>j' , 'which_key_ignore' ]
let g:which_key_map['k'] = [ '<C-w>k' , 'which_key_ignore' ]
let g:which_key_map['l'] = [ '<C-w>l' , 'which_key_ignore' ]

let g:which_key_map['g'] = [ '<C-w>s' , 'Horizontal Split' ]
let g:which_key_map['v'] = [ '<C-w>v' , 'Vertical Split' ]
let g:which_key_map['q'] = [ '<C-w>l' , 'Quit buffer' ]
let g:which_key_map['<Left>'] = [ '<C-w>h' , 'Move Left' ]
let g:which_key_map['<Right>'] = [ '<C-w>l' , 'Move Right' ]
let g:which_key_map['<Up>'] = [ '<C-w>k' , 'Move Up' ]
let g:which_key_map['<Down>'] = [ '<C-w>j' , 'Move Down' ]
let g:which_key_map['m'] = [ '<C-i>' , 'Move Backward' ]
let g:which_key_map['n'] = [ '<C-o>' , 'Move Forward' ]
let g:which_key_map[' '] = [ ':tabnew' , 'New tab' ]
let g:which_key_map['o'] = [ ':browse oldfiles' , 'Recently open' ]

let g:which_key_map.s = {
      \ 'name' : '+search' ,
      \ '/' : [':history'     , ' Search History'],
      \ ';' : ['q:'     , 'Previous Commands'],
      \ 'b' : [':buffers'       , 'Total Buffers'],
      \ 'm' : [':marks'       , 'Marks'],
      \ }

let g:which_key_map.x = {
      \ 'name' : '+Exit Modes' ,
      \ 'x' : [':wq'     , ' Normal Exit'],
      \ 'z' : [':q!'     , 'Forced Exit'],
      \ }

call which_key#register('<Space>', "g:which_key_map")

